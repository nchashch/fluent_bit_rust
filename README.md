# Rust bindings for writing fluent-bit output plugins
This crate provides functions and structs necessary for writing a fluent-bit output plugin.
# Usage
`Cargo.toml` for a minimal output plugin:
```toml
[package]
name = "minimal_plugin"
version = "0.1.0"
authors = ["John Doe <john.doe@example.com>"]
edition = "2018"

[lib]
crate-type = ["cdylib"]

[dependencies]
libc = "0.2.55"
fluent_bit_rust = { git = "https://gitlab.com/nchashch/fluent_bit_rust" }
```
`lib.rs` for a minimal output plugin:
```rust
extern crate libc;
extern crate fluent_bit_rust;

use fluent_bit_rust::output;

#[no_mangle]
unsafe fn FLBPluginRegister(def: *mut output::FLBPluginProxyDef) -> libc::c_int {
    return output::FLBPluginRegister(def, "minimal_plugin", "StdOut.rs");
}

#[no_mangle]
unsafe fn FLBPluginInit(plugin: *mut output::FLBOutPlugin) -> libc::c_int {
    return output::FLB_OK;
}

#[no_mangle]
unsafe fn FLBPluginFlush(data: *mut u8, length: libc::c_int, tag: *mut libc::c_char) -> libc::c_int {
    return output::FLB_OK;
}

#[no_mangle]
fn FLBPluginExit() -> libc::c_int {
    return output::FLB_OK;
}
```
After building an output plugin with `cargo build --release` it can be loaded like this:
```sh
bin/fluent-bit -e /path/to/minimal_plugin/target/release/libminimal_plugin.so -i cpu -o minimal_plugin
```
`fluent-bit` must be built with:
```sh
$ cd build/
$ cmake -DFLB_DEBUG=On -DFLB_PROXY_GO=On ../
$ make
```
(see https://github.com/fluent/fluent-bit/blob/master/GOLANG_OUTPUT_PLUGIN.md)
# Examples
## rstdout
https://gitlab.com/nchashch/rstdout

`rstdout` example demonstrates reading data and printing data to stdout in `FLBPluginFlush`
function, using `rmpv` crate for decoding data in MessagePack format.
`rstdout` example after being built with `cargo build --release` can be loaded using:
```sh
bin/fluent-bit -e /path/to/rstdout/target/release/librstdout.so -i cpu -o rstdout
```
## multiinstance
https://gitlab.com/nchashch/multiinstance

`multiinstance` example demonstrates usage of configuration and context (`FLBPluginConfigKey`, `FLBPluginSetContext`, `FLBPluginGetContext`).
`multiinstance` example after being built with `cargo build --release` can be loaded using:
```sh
bin/fluent-bit -e /path/to/multiinstance/target/release/libmultiinstance.so -o multiinstance --config /path/to/multiinstance/src/fluent-bit.conf
```
