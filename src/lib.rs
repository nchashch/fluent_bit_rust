// #![feature(extern_types)]
pub mod output {
    #![allow(non_snake_case)]
    #![allow(non_camel_case_types)]

    use libc;
    use std::ffi::{CStr, CString};

    // Define constants matching Fluent Bit core
    pub static FLB_ERROR: libc::c_int = 0;
    pub static FLB_OK: libc::c_int = 1;
    pub static FLB_RETRY: libc::c_int = 2;
    pub static FLB_PROXY_OUTPUT_PLUGIN: libc::c_int = 2;
    pub static FLB_PROXY_GOLANG: libc::c_int = 11;

    #[repr(C)]
    pub struct struct_flb_plugin_proxy_def {
        pub r#type: libc::c_int,
        pub proxy: libc::c_int,
        pub flags: libc::c_int,
        pub name: *mut libc::c_char,
        pub description: *mut libc::c_char
    }

    #[repr(C)]
    pub struct flb_api {
        pub output_get_property: extern "C" fn(*const libc::c_char, *const libc::c_void) -> *const libc::c_char,
    }

    #[repr(C)]
    pub struct flb_plugin_proxy_context {
        pub remote_context: *mut libc::c_void,
    }

    #[repr(C)]
    pub struct flb_output_instance;

    #[repr(C)]
    pub struct struct_flbgo_output_plugin {
        pub __: *mut libc::c_void,
        pub api: *mut flb_api,
        pub o_ins: *mut libc::c_void,
        pub context: *mut flb_plugin_proxy_context,
    }

    pub type FLBPluginProxyDef = struct_flb_plugin_proxy_def;
    pub type FLBOutPlugin = struct_flbgo_output_plugin;

    // When the FLBPluginInit is triggered by Fluent Bit, a plugin context
    // is passed and the next step is to invoke this FLBPluginRegister() function
    // to fill the required information: type, proxy type, flags name and
    // description.
    pub unsafe fn FLBPluginRegister(def: *mut FLBPluginProxyDef, name: &str, desc: &str) -> libc::c_int {
        let name = CString::new(name).unwrap();
        let desc = CString::new(desc).unwrap();
        let p = def;
	      (*p).r#type = FLB_PROXY_OUTPUT_PLUGIN;
	      (*p).proxy = FLB_PROXY_GOLANG;
	      (*p).flags = 0;
	      (*p).name = name.into_raw();
	      (*p).description = desc.into_raw();
	      return 0;
    }

    // Release resources allocated by the plugin initialization
    pub unsafe fn FLBPluginUnregister(def: *mut FLBPluginProxyDef) {
	      let p = def;
        // Objects created by CString::from_raw() should free memory after going out of scope.
        CString::from_raw((*p).name);
        CString::from_raw((*p).description);
    }

    pub unsafe fn FLBPluginConfigKey(plugin: *mut FLBOutPlugin, key: &str) -> Option<String> {
        let key = CString::new(key).unwrap();
        let p = plugin;
        let output_get_property = (*(*p).api).output_get_property;
        let string = output_get_property(key.as_ptr(), (*p).o_ins);
        if !string.is_null() {
            let cstr = CStr::from_ptr(string);
            let _str: &str = cstr.to_str().unwrap();
            let string: String = _str.to_owned();
            return Some(string);
        } else {
            return None;
        }
    }

    pub unsafe fn FLBPluginSetContext(plugin: *mut FLBOutPlugin, ctx: &str) {
        let ctx = CString::new(ctx).unwrap();
	      let p = plugin;
	      (*(*p).context).remote_context = ctx.into_raw() as *mut libc::c_void;
    }

    pub unsafe fn FLBPluginGetContext(i: *mut libc::c_void) -> String {
        let result = CStr::from_ptr(i as *mut libc::c_char);
        let result: &str = result.to_str().unwrap();
        let result: String = result.to_owned();
        return result;
    }
}
